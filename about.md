# About GitLab R&eacute;sum&eacute; <small>[Back to Home](/)</small>

---

A service that creates a résumé based on your GitLab repos/activity, inspired by [GitHub Résumé](https://github.com/resume/resume.github.com). Great for all the tech-savy bosses who want to have a quick view of a person's GitLab activity, before the interview.

R&eacute;sum&eacute;s are generated automatically using public information from the developer's GitLab account. The repositories are ordered by popularity based on a very simple popularity heuristic that defines the popularity of a repository by its sum of watchers and forks.

## Limitations

The following is a list of current limitations. Found another limitation? Edit [this file](https://gitlab.com/resume/resume.gitlab.io/blob/master/about.md) on GitLab.
#### 1. R&eacute;sum&eacute;s can only be made by the user that it is being made for. </h4>
Due to the nature of GitLab's API, you can only create a r&eacute;sum&eacute; for your own account.
#### 2. You must use your API token to login.
Due to the fact that every API request must be authenticated, we must ask for your API token. WE DO NOT KEEP YOUR TOKEN! Your token is stored in your cookies. Take a look at the code if you are afraid of that.
#### 3. The generator assumes that your private GitLab instance is served over HTTPS, which it should be.

## Open Source
GitLab R&eacute;sum&eacute; is an open source project.

#### Repository URL
[resume/resume.gitlab.io](https://gitlab.com/resume/resume.gitlab.io)

#### License
[MIT](https://gitlab.com/resume/resume.gitlab.io/blob/master/LICENSE)

#### Contribution
The contribution guide can be found [here](https://gitlab.com/resume/resume.gitlab.io/blob/master/CONTRIBUTING.md).

## Found an Issue?
You can report issues on our issue tracker which can be found [here](https://gitlab.com/resume/resume.gitlab.io/issues).


<br/><br/>
Built by [Filiosoft OSS](https://filiosoft.net), a sub-project of [Filiosoft, LLC](https://filiosoft.com).