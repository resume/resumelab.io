angular.module('app.services', [])
    .service('gitlabApi', function ($http) {
        var apiVersion = 'v4';
        return {
            get: function (endpoint, glApi, token) {
                return $http.get('https://' + glApi + '/api/' + apiVersion + endpoint, {
                        headers: {
                            'PRIVATE-TOKEN': token,
                        }
                    })
                    .then(function (data) {
                        return data;
                    })
                    .catch(function (err) {
                        console.error(err);
                        throw err;
                    });
            }
        };
    });