angular.module('app.controllers', ['app.services'])
    .controller('AppCtrl', function ($scope, gitlabApi, $location, glApi, $cookies, $q) {
        console.log('AppCtrl running...');
        $scope.complete = false;
        $scope.address = '';

        // DEV CONFIG - DO NOT COMMIT THIS
        //$scope.address = 'gitlab.filiosoft.com';
        //$scope.username = 'nprail'
        // DEV CONFIG - DO NOT COMMIT THIS

        $scope.submit = function (username, privatetoken, address, visibility) {
            if (!$scope.address) {
                $scope.address = 'gitlab.com';

            }
            // set token cookie
            if (!$cookies.get('gitlabToken-' + $scope.address)) {
                $cookies.put('gitlabToken-' + $scope.address, privatetoken);
            }


            if ($scope.address === 'gitlab.com') {
                // if the instance is gitlab.com, check to make sure the user is opted in
                checkStarred().then(function (res) {
                    if (res) {
                        // if resume is starred, generate the resume
                        generate();
                    } else {
                        // if resume is NOT starred, ask the user to opt-in
                        $location.path('optin');
                    }
                }).catch(function (err) {
                    onError(err);
                });
            } else {
                // if the instance is NOT gitlab.com, generate the resume
                generate();
            }

            function checkStarred() {
                $scope.loading = true;
                var defered = $q.defer();

                gitlabApi.get('/projects?starred=true', $scope.address, $cookies.get('gitlabToken-' + $scope.address)).then(function (res) {
                    var starred;
                    angular.forEach(res.data, function (repo, key) {
                        // check if repo is resume
                        if (repo.path_with_namespace === 'resume/resume.gitlab.io') {
                            starred = true;
                        }
                        // resolve when done
                        if (key === res.data.length - 1) {
                            defered.resolve(starred);
                        }
                    });

                }).catch(function (err) {
                    defered.reject(err);
                });

                return defered.promise;
            }

            function generate() {
                if ($scope.address == 'gitlab.com') {
                    if (visibility) {
                        return $location.path(username).search({
                            visibility: visibility
                        });
                    }
                    return $location.path(username);
                } else {
                    if (visibility) {
                        return $location.path('private/' + $scope.address + '/' + username).search({
                            visibility: visibility
                        });
                    }
                    return $location.path('private/' + $scope.address + '/' + username);
                }
            };
        }

        var onError = function (err) {
            $scope.error = 'Sorry, we could not fetch the data. :( ERROR MESSAGE: ' + err;
            console.log(err);
        };
    })
    .controller('DetailsCtrl', function ($scope, $routeParams, gitlabApi, $cookies) {
        console.log('DetailsCtrl running...');
        $scope.complete = false;


        var username = $routeParams.userName;
        var visibility;
        var apiAddress;
        if ($routeParams.visibility) {
            visibility = $routeParams.visibility;
        } else {
            visibility = 'public';
        }
        if ($routeParams.address) {
            apiAddress = $routeParams.address;
        } else {
            apiAddress = 'gitlab.com';
        }
        var privatetoken = $cookies.get('gitlabToken-' + apiAddress);

        $scope.print = function () {
            window.print();
            return false;
        };

        var getBasicInfo = gitlabApi.get('/users/?username=' + username, apiAddress, privatetoken).then(function (res) {
            $scope.user = res.data.shift();
            getProfile($scope.user.id);
            getRepos($scope.user.username);
            getMemberProjects();
            getGroups();
        }).catch(function (err) {
            onError(err);
        });

        var getProfile = function (id) {
            gitlabApi.get('/users/' + id, apiAddress, privatetoken).then(function (res) {
                $scope.user = res.data;
                console.info('Getting profile...');
                $scope.profileComplete = true;
            }).catch(function (err) {
                onError(err);
            });
        };

        var getRepos = function (username) {
            gitlabApi.get('/projects?visibility=' + visibility + '&owned=true&search=' + username, apiAddress, privatetoken).then(function (res) {
                $scope.repos = res.data;
                console.info('Getting projects...');
                $scope.repoComplete = true;
            }).catch(function (err) {
                onError(err);
            });
        };

        var getGroups = function (username) {
            gitlabApi.get('/groups?visibility=' + visibility, apiAddress, privatetoken).then(function (res) {
                $scope.groups = res.data;
                console.info('Getting groups...');
                $scope.groupComplete = true;
            }).catch(function (err) {
                onError(err);
            });
        };

        var getMemberProjects = function () {
            gitlabApi.get('/projects?visibility=' + visibility + '&membership=true', apiAddress, privatetoken).then(function (res) {
                $scope.projects = res.data;
                angular.forEach(res.data, function (value, keyProj) {
                    var id = value.id;
                    if (value.default_branch) {
                        getCommits(value.id).then(function (res) {
                            angular.forEach(res, function (value, key) {
                                if ($scope.user.name === value.name) {
                                    $scope.projects[keyProj].user_commits = value.commits;
                                }
                            });
                        });
                    }
                });
                console.info('Getting contributed projects...');
                $scope.contrComplete = true;
            }).catch(function (err) {
                onError(err);
            });
        };

        var getCommits = function (id) {
            return gitlabApi.get('/projects/' + id + '/repository/contributors', apiAddress, privatetoken).then(function (res) {
                console.info('Getting commit count...');
                $scope.commitsComplete = true;
                return res.data;
            }).catch(function (err) {
                onError(err);
            });
        };

        var onError = function (err) {
            console.error(err);
            $scope.error = 'Sorry, we could not fetch the data. :( ERROR MESSAGE: ' + err.data.message;
        };
    })
    .controller('AboutCtrl', function ($scope, $http, $sce) {
        var converter = new showdown.Converter()
        $http.get('../about.md')
            .then(function (res) {
                $scope.html = $sce.trustAsHtml(converter.makeHtml(res.data));
            })
            .catch(function (err) {
                console.error(err);
            });
    });