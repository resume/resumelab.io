angular.module('app', ['ngRoute', 'ngCookies', 'app.controllers', 'app.services'])
    .value('glApi', 'https://gitlab.com/api/v4/')
    .config(function ($routeProvider, $locationProvider) {
        $locationProvider.hashPrefix('');
        $routeProvider
            .when('/', {
                controller: 'AppCtrl as appctrl',
                templateUrl: 'views/home.html',
            })
            .when('/about', {
                controller: 'AboutCtrl as aboutctrl',
                templateUrl: 'views/about.html',
            })
            .when('/optin', {
                templateUrl: 'views/optin.html',
            })
            .when('/private/:address/:userName', {
                controller: 'DetailsCtrl as detailsctrl',
                templateUrl: 'views/details.html',
            })
            .when('/:userName', {
                controller: 'DetailsCtrl as detailsctrl',
                templateUrl: 'views/details.html',
            })
            .otherwise({
                redirectTo: '/'
            });
    });