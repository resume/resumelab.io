# [GitLab Résumé](https://resume.filiosoft.net/) [![build status](https://gitlab.com/resume/resume.gitlab.io/badges/master/build.svg)](https://gitlab.com/resume/resume.gitlab.io/commits/master) [![license](https://img.shields.io/badge/license-MIT%20License-blue.svg)](https://gitlab.com/resume/resume.gitlab.io/blob/master/LICENSE)

**A service that creates a résumé based on your GitLab repos/activity, inspired by [GitHub Résumé](https://github.com/resume/resume.github.com)**


To view your résumé, follow the instructions on the [home page](https://resume.gitlab.io/).

Great for all the tech-savy bosses who want to have a **quick view** of a person's GitLab activity, _before the interview_.

### Development

To run the app in development mode:

    $ npm install
    $ gulp serve

### Limitations
See the [About page](https://resume.gitlab.io/#/about#limits) for info on the limitations. 

### Authors
- Noah Prail - *Maintainer* - [@nprail](https://gitlab.com/nprail)

See also the list of [contributors](https://gitlab.com/resume/resume.gitlab.io/graphs/master) who participated in this project.

### License
[MIT](https://gitlab.com/resume/resume.gitlab.io/blob/master/LICENSE)

### Contributing
See [CONTRIBUTING.md](https://gitlab.com/resume/resume.gitlab.io/blob/master/CONTRIBUTING.md)