'use strict';

const gulp = require('gulp');
const jshint = require('gulp-jshint');
const browserSync = require('browser-sync').create();

const paths = {
    allJs: ['./js/**/*.js'],
    allHtml: ['**/*.html'],
    allCss: ['./css/**/*.css']
}

// Static Server
gulp.task('serve', () => {

    browserSync.init({
        server: "./"
    });
});

gulp.task('default', ['watch']);

gulp.task('watch', ['serve'], () => {
    gulp.watch(paths.allCss).on('change', browserSync.reload);
    gulp.watch(paths.allJs).on('change', browserSync.reload);
    gulp.watch(paths.allHtml).on('change', browserSync.reload);
});

// JShint and JSCS
gulp.task('vet', () => {
    return gulp.src(paths.allJs)
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish', {
            verbose: true
        }))
        .pipe(jshint.reporter('gulp-jshint-html-reporter', {
            filename: __dirname + '/tests/jshint-output.html',
            createMissingFolders: true
        }));
});